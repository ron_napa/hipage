//
//  MainPageViewController.swift
//  HiPage
//
//  Created by Ronald Napa on 21/07/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import UIKit

class MainPageViewController: UIViewController {

    @IBOutlet weak var jobsTableView: UITableView!
    @IBOutlet weak var closedJobIndicatorView: UIView!
    @IBOutlet weak var openJobIndicatorView: UIView!
    
    var jobsTableData : [Job] = []
    var currentJobLoading : Job!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideBothIndicator()
        openJobIndicatorView.backgroundColor = self.hexStringToUIColor(hex: "FB7B12")
        
        SessionManager.downloadJson()
        jobsTableData = SessionManager.sharedInstance.jobPool
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - Button Actions
    
    @IBAction func showOpenJobs(_ sender: Any) {
        hideBothIndicator()
        openJobIndicatorView.backgroundColor = UIColor.orange
    }
    
    @IBAction func showClosedJobs(_ sender: Any) {
        hideBothIndicator()
        closedJobIndicatorView.backgroundColor = UIColor.orange
    }
    
    //MARK: - Private Function
    
    func hideBothIndicator(){
        openJobIndicatorView.backgroundColor = UIColor.clear
        closedJobIndicatorView.backgroundColor = UIColor.clear
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}

extension MainPageViewController: UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - TableView Data Source
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model:Job! = self.jobsTableData[indexPath.row] as Job!
        currentJobLoading = model
        let cell = tableView.dequeueReusableCell(withIdentifier: "jobCell") as! JobTableViewCell
        cell.labelTitle.text = model.category
        cell.labelDate.text = model.postedDate
        cell.labelStatus.text = model.status
        
        if(model.connectedBusinesses.count == 0){
            cell.labelHired.text = "Connecting you with businesses"
        }else{
            cell.labelHired.text = "You have hired \(model.connectedBusinesses.count) businesses"
        }
        cell.bussinessCount = model.connectedBusinesses.count
        cell.connectedCollectionView.reloadData()
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jobsTableData.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

extension MainPageViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.currentJobLoading.connectedBusinesses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let content:Business = self.currentJobLoading.connectedBusinesses[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "connectedCell",for: indexPath) as! ConnectedCollectionViewCell
        
        if(content.isHired){
            cell.labelHired.isHidden = false
        }else{
            cell.labelHired.isHidden = true
        }
        
        ImageDownloader.downloadImage(path: content.thumbnailUrl, url: content.thumbnailUrl, completion: { (imageResult) in
            cell.imageViewThumbnail.image = imageResult
            cell.imageViewThumbnail.layer.cornerRadius = cell.imageViewThumbnail.frame.size.height / 2
            cell.imageViewThumbnail.clipsToBounds = true
        })
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = UIScreen.main.bounds
        return CGSize(width: screenSize.width/4, height: screenSize.width/4);
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
}
