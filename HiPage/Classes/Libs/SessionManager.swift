//
//  SessionManager.swift
//  HiPage
//
//  Created by Ronald Napa on 21/07/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftSpinner

class SessionManager : NSObject {
    
    static var alertLoading : UIAlertController!
    static var request: Alamofire.Request?
    
    static let sharedInstance = SessionManager()
    var jobPool : [Job] = []
    
    static func downloadJson(){
        
        let pdfUrl = "https://s3-ap-southeast-2.amazonaws.com/hipgrp-assets/tech-test/jobs.json"
        let sliceArray = pdfUrl.characters.split{$0 == "/"}.map(String.init)
        let pdfLocalDir = sliceArray[sliceArray.count-1]
        
        let documentDirectoryURL =  try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let filPath = NSURL.init(string: "\(documentDirectoryURL)\(pdfLocalDir)")
        let data = NSData(contentsOf: filPath! as URL)
        
        if(data != nil){
            
            readJson()
            
        }else{
            
            alertLoading = UIAlertController(title: "Downloading", message: "\(pdfLocalDir)...", preferredStyle: .alert)
            
            alertLoading.view.tintColor = UIColor.black
            
            UIApplication.topViewController()?.present(alertLoading, animated: true, completion: nil)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                self.request?.cancel()
                self.alertLoading.dismiss(animated: false, completion: nil)
            }
            
            alertLoading.addAction(cancelAction)
            
            let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
            
            let utilityQueue = DispatchQueue.global(qos: .utility)
            
            request = Alamofire.download(pdfUrl, to: destination)
                .downloadProgress(queue: utilityQueue) { progress in
                    print(progress)
                    print("Download Progress: \(progress.fractionCompleted)")
                    
                    self.alertLoading.message = "\(pdfLocalDir)... \n \(progress.fractionCompleted * 100)MB%"
                    
                }
                .responseData { response in
                    print(response)
                    if let data = response.result.value {
                        self.readJson()
                    }
                    
                    self.alertLoading.dismiss(animated: false, completion: nil)
            }
            
        }
    }
    
    static func readJson() {
        do {
            
            let documentDirectoryURL =  try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let filPath = NSURL.init(string: "\(documentDirectoryURL)\("jobs.json")")
            let data = try Data(contentsOf: filPath! as URL)
            
            if(data != nil){
                //let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    
                    /*
                     "jobId": 423421,
                     "category": "Electricians",
                     "postedDate": "2017-04-13",
                     "status": "In Progress",
                     "connectedBusinesses": [
                     {
                     "businessId": 203213,
                     "thumbnail": "https://assets.homeimprovementpages.com.au/images/hui/avatars/a.png",
                     "isHired": false
                    */
                    
                    let jobs = object["jobs"] as! [Any]
                    
                    for job in jobs{
                        let fetchedJob = job as! [String : Any]
                        
                        let newJob = Job()
                        newJob.jobID = fetchedJob["jobId"] as! Int
                        newJob.category = fetchedJob["category"] as! String
                        newJob.postedDate = fetchedJob["postedDate"] as! String
                        newJob.status = fetchedJob["status"] as! String
                        
                        if(fetchedJob["connectedBusinesses"] is NSNull){
                            
                        }else{
                            let jobConnect = fetchedJob["connectedBusinesses"] as! [Any]
                            
                            for connect in jobConnect{
                                let fetchedConnect = connect as! [String : Any]
                                
                                let newConnect = Business()
                                newConnect.businessId = fetchedConnect["businessId"] as! Int
                                newConnect.thumbnailUrl = fetchedConnect["thumbnail"] as! String
                                newConnect.isHired = fetchedConnect["isHired"] as! Bool
                                
                                newJob.connectedBusinesses.append(newConnect)
                                
                            }
                        }
                        
                        self.sharedInstance.jobPool.append(newJob)
                        
                    }
                    
                    
                } else if let object = json as? [Any] {
                    
                    print(object)
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
}
