//
//  ImageDownloader.swift
//  HiPage
//
//  Created by Ronald Napa on 21/07/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class ImageDownloader : NSObject {
    static let shareInstance  = ImageDownloader()
    let imageCache = AutoPurgingImageCache()

    class func downloadImage(path:String!, url:String!, completion:@escaping (_ result: UIImage) -> Void){
        
        let cacheImage = self.shareInstance.imageCache.image(withIdentifier: path)
        
        if(cacheImage != nil){
            completion(cacheImage!)
        }else{
            Alamofire.request(url).responseImage { response in
                
                if let image = response.result.value {
                    self.shareInstance.imageCache.add(image, withIdentifier: path)
                    completion(image)
                }
            }
        }
        
    }
}
