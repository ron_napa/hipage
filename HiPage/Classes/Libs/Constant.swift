//
//  Constant.swift
//  HiPage
//
//  Created by Ronald Napa on 21/07/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import Foundation

class Constant: NSObject {
    
    let apiEndPoint : String = "https://s3-ap-southeast-2.amazonaws.com/hipgrp-assets/tech-test/jobs.json"
    
}
