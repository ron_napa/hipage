//
//  Job.swift
//  HiPage
//
//  Created by Ronald Napa on 21/07/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import Foundation

class Job: NSObject {
    
    var jobID : Int = 0
    var category : String = ""
    var postedDate : String = ""
    var status : String = ""
    var connectedBusinesses : [Business] = []
    
}
