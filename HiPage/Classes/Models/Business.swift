//
//  Business.swift
//  HiPage
//
//  Created by Ronald Napa on 21/07/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import Foundation

class Business: NSObject {
    
    var businessId : Int = 0
    var thumbnailUrl : String = ""
    var isHired : Bool = false
    
}
