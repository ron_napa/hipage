//
//  ConnectedCollectionViewCell.swift
//  HiPage
//
//  Created by Ronald Napa on 21/07/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import UIKit

class ConnectedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewThumbnail: UIImageView!
    @IBOutlet weak var labelHired: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
