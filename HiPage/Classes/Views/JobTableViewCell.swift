//
//  JobTableViewCell.swift
//  HiPage
//
//  Created by Ronald Napa on 21/07/2017.
//  Copyright © 2017 Ronald Napa. All rights reserved.
//

import UIKit

class JobTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelHired: UILabel!
    
    @IBOutlet weak var connectedCollectionView: UICollectionView!
    @IBOutlet weak var connectedCollectionViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var closeButton: UIButton!
    
    var bussinessCount:Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if(bussinessCount == 0){
            connectedCollectionViewConstraint.constant = 0
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func showCloseJobButton(_ sender: Any) {
        if(closeButton.isHidden){
            closeButton.isHidden = false
        }else{
            closeButton.isHidden = true
        }
    }
    
    @IBAction func closeButtonWasPressed(_ sender: Any) {
        
        
    }

}
